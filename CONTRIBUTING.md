# Contributing to libbiomedit

## Development Setup

### Installation

After cloning the repository install the package in editable mode
(it allows trying your changes without reinstalling the package),
together with the development dependencies.

```bash
pip install -e .[dev]
```

### Code style

This project uses [ruff](https://astral.sh/ruff) for code formatting.

```sh
ruff format --check .
```

For your convenience, you can install a pre-commit hook for ruff. For
pre-commit installation instruction refer to the
[documentation](https://pre-commit.com/#install).

```bash
pre-commit install
```

In addition we use additional tools for static code analysis.
You can run them locally with.

```bash
bandit -r libbiomedit
mypy libbiomedit test
ruff check libbiomedit test
```

### Testing

To test the code locally, run:

```bash
tox
```

## Commit Message Guidelines

This project follows the [Angular commit message guidelines](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#-commit-message-guidelines)
for its commit messages.

### Template

Fields shown in `[square brackets]` are optional.

```text
type[(scope)]: subject line - max 100 characters

[body] - extended description of commit that can stretch over
multiple lines. Max 100 character per line.

[footer] - links to issues with (Closes #, Fixes #, Relates #) and BREAKING CHANGE:
```

### Examples

```text
feat(crypt): add support to validate multiple keys at once.

The validated_keys_by_ids() function now takes a iterator as input for the 'key_ids' parameter
instead of a single key. This allows validating multiple keys at once.

BREAKING CHANGE: validated_keys_by_ids() now takes a iterator as input for its 'key_ids' parameter.
    To migrate your code, pass an iterator object to validated_keys_by_ids() instead of a single
    key.

        Before:

        validated_keys_by_ids(key_id = str, ...)

        After:

        validated_keys_by_ids(key_ids = Iterator[str], ...)

Closes #7, #23, Related #9
```

```text
fix(crypt): refresh keys from default keyserver if they miss origin

Add the possibility to provide a default keyserver from which to refresh/download keys. The key
refresh procedure now tries to download keys from, in this order:
1. a default keyserver (keyserver_url), if specified.
2. the keyserver indicated as 'origin' in the key, if any.

Keys that could not be refreshed do no longer trigger an error, instead, a warning is displayed.

Closes #5
```

```text
docs(CONTRIBUTING): add examples of conventional commit messages

Add a few examples of conventional commit messages to CONTRIBUTING.md, so that people don't have
to click on the "Angular commit message guidelines" link to read the full specifications.
Add a concise summary of the guidelines to provider a reminder of the main types and keywords.

Closes #6
```

### Type and keywords summary

**Type:** the following types are allowed. Only types shown in **bold** are automatically added to
the changelog (and those containing the `BREAKING CHANGE:` keyword):

- **feat**: new feature
- **fix**: bug fix
- **metadata**: changes to the MetaData object
- build: changes that affect the build system or external dependencies.
- ci: changes to CI configuration files and scripts.
- docs: documentation only changes
- perf: code change that improves performance.
- refactor: code change that neither fixes a bug nor adds a feature
- style: change in code formatting only (no effect on functionality).
- test: change in unittest files only.

**Scope:**

- name of the file/functionality/workflow affected by the commit.

**Subject line:**

- one line description of commit with max 100 characters.
- use the imperative form, e.g. "add new feature" instead of "adds new feature" or
  "added a new feature".
- no "." at the end of subject line.

**body:**

- Extended description of commit that can stretch over multiple lines.
- Max 100 characters per line.
- Explain things such as what problem the commit addresses, background info on why the change
  was made, alternative implementations that were tested but didn't work out.

**footer:**

- Reference to git issue with `Closes/Close`, `Fixes/Fix`, `Related`.
- Location for `BREAKING CHANGE:` keyword. Add this keyword followed by a description of what
  the commit breaks, why it was necessary, and how users should port their code to adapt to the
  new version.

### Commit messages and auto-versioning

The following rules are applied by the auto-versioning system to modify the version number when a
new commit is pushed to the `main` branch:

- Keyword `BREAKING CHANGE:`: increases the new major digit, e.g. 1.1.1 -> 2.0.0
- Type `feat`: increases the minor version digit, e.g. 1.1.1 -> 1.2.0
- Type `fix`: increases the patch digit, e.g. 1.1.1 -> 1.1.2

**Note:** an exception to the behavior of `BREAKING CHANGE:` is for pre-release versions (i.e.
0.x.x). In that case, a `BREAKING CHANGE:` keyword increases the minor digit rather than the
major digit. For more details, see the
[conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) and the
[semantic versioning](https://semver.org/spec/v2.0.0.html) specifications.
