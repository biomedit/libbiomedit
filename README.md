[![pipeline status](https://gitlab.com/biomedit/libbiomedit/badges/main/pipeline.svg)](https://gitlab.com/biomedit/libbiomedit/-/commits/main)
[![coverage report](https://gitlab.com/biomedit/libbiomedit/badges/main/coverage.svg)](https://gitlab.com/biomedit/libbiomedit/-/commits/main)
[![Code style: ruff](https://img.shields.io/badge/code%20style-Ruff-000000.svg)](https://astral.sh/ruff)
[![python version](https://img.shields.io/pypi/pyversions/libbiomedit.svg)](https://pypi.org/project/libbiomedit)
[![license](https://img.shields.io/badge/License-LGPLv3-blue.svg)](https://www.gnu.org/licenses/lgpl-3.0)
[![latest version](https://img.shields.io/pypi/v/libbiomedit.svg)](https://pypi.org/project/libbiomedit)

# libbiomedit

Shared library for BioMedIT projects.
