#!/usr/bin/env python3

import ast
import logging
import subprocess
import sys
from difflib import ndiff
from typing import Iterable, List, Optional, Sequence, Tuple

logging.basicConfig(format="[%(levelname)s] %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

METADATA_MODULE = "libbiomedit/metadata.py"
METADATA_CLASS_NAME = "MetaData"
METADATA_COMMIT_TYPE = "metadata"


def main() -> None:
    latest_tag = get_output(["git", "describe", "--tags", "--abbrev=0"])
    commits = get_output(["git", "rev-list", f"HEAD...{latest_tag}"]).split()
    bad_commits = [commit for commit in commits if not verify_commit(commit)]
    if bad_commits:
        sys.exit(1)


def verify_commit(commit: str) -> bool:
    logger.info("Processing commit: %s", commit)
    file_current = get_output(["git", "show", f"{commit}:{METADATA_MODULE}"])
    file_prev = get_output(["git", "show", f"{commit}~:{METADATA_MODULE}"])
    changes = tuple(find_changes(file_current, file_prev))
    if changes:
        wrong_metadata_version = not is_newer(
            get_metadata_version(get_metadata_class_fields(file_prev)),
            get_metadata_version(get_metadata_class_fields(file_current)),
        )
        missing_metadata_header = not get_commit_title(commit).startswith("metadata")
        if wrong_metadata_version:
            logger.warning("MetaData class changed without incrementing version number")
        if missing_metadata_header:
            logger.warning(
                "MetaData class changed in a wrong commit type. Commit title "
                "must start with '%s:'",
                METADATA_COMMIT_TYPE,
            )
        bad_commit = wrong_metadata_version or missing_metadata_header
        for msg in changes:
            logger.log(
                logging.WARNING if bad_commit else logging.INFO,
                msg,
            )
        return not bad_commit
    return True


def get_metadata_class_fields(source: str) -> List[ast.AnnAssign]:
    return [
        class_node
        for node in ast.parse(source).body
        if isinstance(node, ast.ClassDef) and node.name == METADATA_CLASS_NAME
        for class_node in node.body
        if isinstance(class_node, ast.AnnAssign)
    ]


def get_metadata_version(nodes: Sequence[ast.AnnAssign]) -> Optional[Tuple[int, ...]]:
    try:
        return next(
            tuple(int(x) for x in node.value.value.split("."))
            for node in nodes
            if isinstance(node.target, ast.Name)
            and node.target.id == "version"
            and isinstance(node.value, ast.Constant)
        )
    except StopIteration:
        return None


def is_newer(v1: Optional[Tuple[int, ...]], v2: Optional[Tuple[int, ...]]) -> bool:
    return not (v1 is None or v2 is None) and v2 > v1


def find_changes(current: str, prev: str) -> Iterable[str]:
    nodes_current = get_metadata_class_fields(current)
    nodes_prev = get_metadata_class_fields(prev)
    if len(nodes_current) != len(nodes_prev):
        yield (
            f"Different number of fields, current {len(nodes_current)}, "
            f"previous {len(nodes_prev)}"
        )
    for current_node, prev_node in zip(nodes_current, nodes_prev):
        current_str = ast.dump(current_node)
        prev_str = ast.dump(prev_node)
        if current_str != prev_str:
            yield (
                "Field has changed\n"
                + "".join(ndiff([prev_str + "\n"], [current_str + "\n"])).strip()
            )


def get_output(cmd: List[str]) -> str:
    return subprocess.run(cmd, check=True, capture_output=True).stdout.decode().strip()


def get_commit_title(commit: str) -> str:
    return get_output(["git", "log", "--pretty=format:%s", "-n", "1", commit])


if __name__ == "__main__":
    main()
