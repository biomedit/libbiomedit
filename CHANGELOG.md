# Changelog

All notable changes to this project will be documented in this file.

## [0.6.5](https://gitlab.com/biomedit/libbiomedit/-/releases/0%2E6%2E5) - 2024-05-31

[See all changes since the last release](https://gitlab.com/biomedit/libbiomedit/compare/0%2E6%2E4...0%2E6%2E5)


### 🐞 Bug Fixes

- **metadata:** Limit fractional seconds precision to microseconds ([a2e695b](https://gitlab.com/biomedit/libbiomedit/commit/a2e695b898f2e5ef8e2f469ef20c049658e4e740))

## [0.6.4](https://gitlab.com/biomedit/libbiomedit/-/releases/0%2E6%2E4) - 2024-05-28

[See all changes since the last release](https://gitlab.com/biomedit/libbiomedit/compare/0%2E6%2E3...0%2E6%2E4)

### 🐞 Bug Fixes

- **metadata:** Make datetime parsing ISO 8601 compatible ([e754046](https://gitlab.com/biomedit/libbiomedit/commit/e75404696388e5f09115da3f1e7d031805f892ac)), Close #34

### 🧹 Refactoring

- Replace pylint with ruff ([fbe21e3](https://gitlab.com/biomedit/libbiomedit/commit/fbe21e326661fcb862d788f39849804f0f6db523))

## [0.6.3](https://gitlab.com/biomedit/libbiomedit/compare/0.6.2...0.6.3) (2022-10-31)

### Features

- **portal:** add (optional) file size field to dpkg status request ([131354e](https://gitlab.com/biomedit/libbiomedit/commit/131354e07bfe317f697ce40532c620f8ffdcbd7d))

## [0.6.2](https://gitlab.com/biomedit/libbiomedit/compare/0.6.1...0.6.2) (2022-09-23)

### Bug Fixes

- **crypt:** fix signee key auto-downoad ([88edaf8](https://gitlab.com/biomedit/libbiomedit/commit/88edaf841647f542f45c11023d7244205ed72bea)), closes [#30](https://gitlab.com/biomedit/libbiomedit/issues/30)
- **crypt:** use fingerprints to detect subkeys ([84b566b](https://gitlab.com/biomedit/libbiomedit/commit/84b566b8f70f5acad3862e5c916c2c0e82dae2fc)), closes [#31](https://gitlab.com/biomedit/libbiomedit/issues/31)

## [0.6.1](https://gitlab.com/biomedit/libbiomedit/compare/0.6.0...0.6.1) (2022-08-25)

### Features

- auto-retrieve signee_fingerprint in verify_metadata_signature ([fb5330c](https://gitlab.com/biomedit/libbiomedit/commit/fb5330c7e6da9d518b947770f220aeccd6dbf515)), closes [#20](https://gitlab.com/biomedit/libbiomedit/issues/20)

## [0.6.0](https://gitlab.com/biomedit/libbiomedit/compare/0.5.0...0.6.0) (2022-08-17)

### ⚠ BREAKING CHANGES

- use BioMedIT portal instead of key signatures

- retrieve_refresh_and_validate_keys replaced with retrieve_and_refresh_keys
- refresh_keys is removed
- key approval verification requires BioMedIT portal

### Features

- add portal module containing PortalApi ([08e34e4](https://gitlab.com/biomedit/libbiomedit/commit/08e34e42108d9eadbd7a5785267862f3dfeebe0a))
- add report_dpkg and check_dpkg to PortalApi ([0b1bf58](https://gitlab.com/biomedit/libbiomedit/commit/0b1bf582f45699f9540185d3dfc53996256b4ffa)), closes [#26](https://gitlab.com/biomedit/libbiomedit/issues/26)
- add support for Python 3.11 ([18b0d42](https://gitlab.com/biomedit/libbiomedit/commit/18b0d42bd5e4b1b0910c43bd5f3f62f7dd43185f)), closes [#22](https://gitlab.com/biomedit/libbiomedit/issues/22)
- add verify_transfer to PortalAPI ([4f4c515](https://gitlab.com/biomedit/libbiomedit/commit/4f4c515dde84a072da51fd5094b96dad7725d663)), closes [#25](https://gitlab.com/biomedit/libbiomedit/issues/25)
- update for VKS - verifying keyserver ([5c8fba5](https://gitlab.com/biomedit/libbiomedit/commit/5c8fba52f40ee28556b6b0f7079ea2b17fcae9d5)), closes [#24](https://gitlab.com/biomedit/libbiomedit/issues/24)

## [0.5.0](https://gitlab.com/biomedit/libbiomedit/compare/0.4.4...0.5.0) (2021-11-16)

### ⚠ BREAKING CHANGES

- **typing:** normalize argument of metadata.hex_str is no longer allowed to be None

### Features

- **typing:** Comply with mypy --strict ([7a8285f](https://gitlab.com/biomedit/libbiomedit/commit/7a8285ff2217f65b8b316768cae305e573d4fd43))

### [0.4.4](https://gitlab.com/biomedit/libbiomedit/compare/0.4.3...0.4.4) (2021-11-03)

### Features

- **lib.deserialize:** Finish (de)serialization of Unions of dataclasses ([b2d48d5](https://gitlab.com/biomedit/libbiomedit/commit/b2d48d5a992c1b21a7049a8897eddf0a97f2297a))
- Move creation of inner transforms to the factory part of a (de)serializer ([815e30a](https://gitlab.com/biomedit/libbiomedit/commit/815e30ae89db15d71d293d0b570c2db76fc0c08e))
- Support for secrets ([971af0e](https://gitlab.com/biomedit/libbiomedit/commit/971af0e83b31544b3037009959a6b8b49135de3e))

## [0.4.3](https://gitlab.com/biomedit/libbiomedit/compare/0.4.2...0.4.3) (2021-10-13)

### Features

- partial support for **future**.annotations ([164fd4b](https://gitlab.com/biomedit/libbiomedit/commit/164fd4bf6e3d087e2dcbc88374069e60b3235447))

### Bug Fixes

- **crypt:** autodownload key before validating metadata signature ([3802104](https://gitlab.com/biomedit/libbiomedit/commit/3802104dce836efdb6b2954e0a4483bf5491bca6)), closes [#21](https://gitlab.com/biomedit/libbiomedit/issues/21)
- update libbiomedit for changes in gpg-lite to KeyCapability (now an Enum object) ([49fe1cb](https://gitlab.com/biomedit/libbiomedit/commit/49fe1cba68646b3c50742f93c10ec260d9e9fdd7))

## [0.4.2](https://gitlab.com/biomedit/libbiomedit/compare/0.4.1...0.4.2) (2021-08-17)

### Bug Fixes

- **crypt:** consider subkey fingerprints when verifying metadata signature ([4bc270d](https://gitlab.com/biomedit/libbiomedit/commit/4bc270d5fdfeadb1f37e66fef037595fea0e283a)), closes [#19](https://gitlab.com/biomedit/libbiomedit/issues/19)

## [0.4.1](https://gitlab.com/biomedit/libbiomedit/compare/0.4.0...0.4.1) (2021-08-16)

### Bug Fixes

- improve metadata signature check error message ([9b11c1f](https://gitlab.com/biomedit/libbiomedit/commit/9b11c1fe164a4b4666b5ae88cf8bdaaaf62e58be)), closes [#18](https://gitlab.com/biomedit/libbiomedit/issues/18)

## [0.4.0](https://gitlab.com/biomedit/libbiomedit/compare/0.3.2...0.4.0) (2021-08-11)

### ⚠ BREAKING CHANGES

- **crypt:** validate_pub_key() and assert_key_is_signed() now have
  a new mandatory argument "gpg_store".

### Bug Fixes

- **crypt:** add support for old GnuPG versions in assert_key_is_signed() ([ba8a8ae](https://gitlab.com/biomedit/libbiomedit/commit/ba8a8ae3cf3950a37882657b5cca6b22b62c860f)), closes [#17](https://gitlab.com/biomedit/libbiomedit/issues/17)

## [0.3.2](https://gitlab.com/biomedit/libbiomedit/compare/0.3.1...0.3.2) (2021-07-29)

### Features

- **crypt:** add option to disable key auto-download ([a7b0624](https://gitlab.com/biomedit/libbiomedit/commit/a7b0624802eb12343cdeea5df06523ce978207d6)), closes [#14](https://gitlab.com/biomedit/libbiomedit/issues/14)

### Bug Fixes

- update libbiomedit for keyserver error handling changes in gpg-lite ([b4955fa](https://gitlab.com/biomedit/libbiomedit/commit/b4955fab108e2222597acc7841b2fd1e129444ad)), closes [#16](https://gitlab.com/biomedit/libbiomedit/issues/16)

## [0.3.1](https://gitlab.com/biomedit/libbiomedit/compare/0.3.0...0.3.1) (2021-07-07)

### Bug Fixes

- improve error handling and messages in retrieve_refresh_and_validate_keys ([b07295c](https://gitlab.com/biomedit/libbiomedit/commit/b07295cad1fbc66c047a95fa9d4130c7b8b513bd)), closes [#11](https://gitlab.com/biomedit/libbiomedit/issues/11)
- use fingerprint when verifying keys ([ff4e66c](https://gitlab.com/biomedit/libbiomedit/commit/ff4e66c9cf8e371a5e325b99197b37d63b3f1c61)), closes [#13](https://gitlab.com/biomedit/libbiomedit/issues/13)

## [0.3.0](https://gitlab.com/biomedit/libbiomedit/compare/0.2.2...0.3.0) (2021-06-11)

### ⚠ BREAKING CHANGES

- retrieve_refresh_and_validate_keys() replaces validated_keys_by_ids()
- validation authority key must be a provided as a full fingerprint
  instead of a long key ID.

### Features

- auto-download and refresh of the key authority's PGP key. ([5f37b5b](https://gitlab.com/biomedit/libbiomedit/commit/5f37b5b458cf7c2c08c34d6ae8b9708a3db01891))

## [0.2.2](https://gitlab.com/biomedit/libbiomedit/compare/0.2.1...0.2.2) (2021-06-01)

### Features

- **crypt.verify_metadata_signature:** accept zip archives ([bb83aec](https://gitlab.com/biomedit/libbiomedit/commit/bb83aec994ff6800c3f871a67047e34c763721a3))

## [0.2.1](https://gitlab.com/biomedit/libbiomedit/compare/0.2.0...0.2.1) (2020-11-05)

## [0.2.0](https://gitlab.com/biomedit/libbiomedit/compare/0.1.0...0.2.0) (2020-08-20)

### ⚠ BREAKING CHANGES

- **metadata:** Remove downward compatibility to metadata v0.5

### Features

- **lib.deserialize:** Implement serialize in favor of dataclasses.asdict ([3022116](https://gitlab.com/biomedit/libbiomedit/commit/3022116a482457074474ca0e023b07197cb62e19))

### Bug Fixes

- **metadata:** Remove downward compatibility to metadata v0.5 ([88334e1](https://gitlab.com/biomedit/libbiomedit/commit/88334e1d95075d2e83ca608acb25454e0d4128ea))

### Metadata

- Make purpose field optional ([ecd99fa](https://gitlab.com/biomedit/libbiomedit/commit/ecd99fa43964a7ca400aa852fba5d09532c53bd7))

## [0.1.0](https://gitlab.com/biomedit/libbiomedit/compare/0.0.9...0.1.0) (2020-08-18)

### ⚠ BREAKING CHANGES

- Removed projectID

### Metadata

- remove projectID ([a86a13e](https://gitlab.com/biomedit/libbiomedit/commit/a86a13ec0bb2541efb59afd96aa1bdabec52652c))

## [0.0.9](https://gitlab.com/biomedit/libbiomedit/compare/0.0.8...0.0.9) (2020-08-14)

### Features

- **crypt.verify_metadata_signature:** Increase verbosity for exceptions ([070f379](https://gitlab.com/biomedit/libbiomedit/commit/070f379fd7c3c96a6a1bb48806bc8b6984884a64))
- **metadata:** add transfer_id and purpose, make projectID optional ([572ea23](https://gitlab.com/biomedit/libbiomedit/commit/572ea23c292c9d44d3a7805adc05db4afdc0c20c)), closes [#4](https://gitlab.com/biomedit/libbiomedit/issues/4)

### Bug Fixes

- **crypt:** make warning message more explicit when key is not signed ([97bff60](https://gitlab.com/biomedit/libbiomedit/commit/97bff60b97c0d22f7c52551663d220e5128ee3fe)), closes [#9](https://gitlab.com/biomedit/libbiomedit/issues/9)
- **crypt.refresh_keys:** Avoid multiple queries for the same key ([6c2423b](https://gitlab.com/biomedit/libbiomedit/commit/6c2423bc33b4014d4dc3d74c0f004cb22cd682e7))
- **crypt.verify_metadata_signature:** Support for windows encoded ascii armored signatures ([8403a49](https://gitlab.com/biomedit/libbiomedit/commit/8403a491a19787a98921d3533c5e667d3bc137d1))

## [0.0.8](https://gitlab.com/biomedit/libbiomedit/compare/0.0.7...0.0.8) (2020-07-30)

### Bug Fixes

- **crypt:** Fix key refreshing ([b2dd0ea](https://gitlab.com/biomedit/libbiomedit/commit/b2dd0ea6854f25c4eb32eb909efb0e6c8f50892f))
- **crypt:** remove duplicated key in the warning message generated by refresh_keys() ([b349bad](https://gitlab.com/biomedit/libbiomedit/commit/b349bad8865b8b012f2a17d5c8aa24b90319f412)), closes [#7](https://gitlab.com/biomedit/libbiomedit/issues/7)

## [0.0.7](https://gitlab.com/biomedit/libbiomedit/compare/0.0.6...0.0.7) (2020-07-24)

### Features

- **crypt.verify_metadata_signature:** Support for ascii armored signatures ([a8a3b3c](https://gitlab.com/biomedit/libbiomedit/commit/a8a3b3ca5c3ed5f112798e1af3df5d243bd8a8bf))

### Bug Fixes

- **crypt:** refresh keys from default keyserver if they miss origin ([098b7d6](https://gitlab.com/biomedit/libbiomedit/commit/098b7d66962930efba18e1847f9a9c1cbd656c6f)), closes [#5](https://gitlab.com/biomedit/libbiomedit/issues/5)

## [0.0.6](https://gitlab.com/biomedit/libbiomedit/compare/0.0.5...0.0.6) (2020-07-13)

### Bug Fixes

- **crypt.verify_metadata_signature:** Propagate url_opener ([195d1ec](https://gitlab.com/biomedit/libbiomedit/commit/195d1ecfda1a37512bc9d8759cde6d084d57b643))

## [0.0.5](https://gitlab.com/biomedit/libbiomedit/compare/0.0.4...0.0.5) (2020-07-09)

### Features

- **crypt:** Support for url_opener parameter ([8c1199b](https://gitlab.com/biomedit/libbiomedit/commit/8c1199bb935b5d155c1442b2affe3cca19ce92d6))

### Bug Fixes

- **crypt.verify_metadata_signature:** Refresh / download signature before verifying ([ca8c839](https://gitlab.com/biomedit/libbiomedit/commit/ca8c839136db62a2809dcd06d7c31a9909019c2e))

## 0.0.4 (2020-07-06)
